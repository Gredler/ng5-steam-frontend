import { Injectable } from '@angular/core';
import { IDeveloper } from './developer';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class DeveloperService {

  private _url: string;

  constructor(private http: HttpClient) {
    this._url = '/api/v1/developer/';
  }

  getDevelopers(): Observable<IDeveloper[]> {
    return this.http.get<IDeveloper[]>(this._url).catch(this.errorHandler);
  }

  getDeveloper(id: number): Observable<IDeveloper> {
    return this.http.get<IDeveloper>(this._url + id).catch(this.errorHandler);
  }

  deleteDeveloper(id: number): Observable<IDeveloper> {
    return this.http.delete<IDeveloper>(this._url + id).catch(this.errorHandler);
  }

  createDeveloper(developer: IDeveloper): Observable<IDeveloper> {
    return this.http.post<IDeveloper>(this._url, developer).catch(this.errorHandler);
  }

  updateDeveloper(developer: IDeveloper): Observable<IDeveloper> {
    return this.http.put<IDeveloper>(this._url + developer.id, developer).catch(this.errorHandler);
  }

  errorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || 'Server Error');
  }
}
