import { Component, OnInit } from '@angular/core';
import { IUser } from '../user';
import { UserService } from '../user.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  public users: IUser[];
  public errorMessage: String;

  constructor(private _userService: UserService, private router: Router, private route: ActivatedRoute) {
    this.errorMessage = '';
  }

  ngOnInit() {
    this._userService.getUsers().subscribe(
      data => this.users = data,
      error => this.errorMessage = error
    );
  }

  showDetails(id: number) {
    this.router.navigate([id + '/view'], {relativeTo: this.route});
  }

  showUpdate(id: number) {
    this.router.navigate([id + '/update'], {relativeTo: this.route});
  }

  deleteUser(id: number) {
    this._userService.deleteUser(id).subscribe(() => this.updateUsers());
  }

  showCreate() {
    this.router.navigate(['create'], {relativeTo: this.route});
  }

  showManageGames(id: number) {
    this.router.navigate([id + '/manage-games'], {relativeTo: this.route});
  }

  updateUsers() {
    this._userService.getUsers().subscribe(
      data => this.users = data,
      error => this.errorMessage = error
    );
  }
}
