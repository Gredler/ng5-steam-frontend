import { Component, OnInit } from '@angular/core';
import { ILanguage } from '../language';
import { LanguageService } from '../language.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-language-detail',
  templateUrl: './language-detail.component.html',
  styleUrls: ['./language-detail.component.scss']
})
export class LanguageDetailComponent implements OnInit {

  public languageId: number;
  public language: ILanguage;
  public errorMessage: string;

  constructor(private _languageService: LanguageService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(
      (params: ParamMap) => (this.languageId = parseInt(params.get('id'), 10))
    );
    this._languageService.getLanguage(this.languageId).subscribe(
      data => this.language = data,
      error => this.errorMessage = error
    );
  }

  goBack() {
    this.router.navigate(['../../'], {relativeTo: this.route});
  }

  deleteLanguage() {
    this._languageService.deleteLanguage(this.languageId).subscribe(() => this.goBack());
  }
}
