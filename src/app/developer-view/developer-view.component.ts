import { Component, OnInit } from '@angular/core';
import { IDeveloper } from '../developer';
import { DeveloperService } from '../developer.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-developer-view',
  templateUrl: './developer-view.component.html',
  styleUrls: ['./developer-view.component.scss']
})
export class DeveloperViewComponent implements OnInit {

  public developerId: number;
  public developer: IDeveloper;
  public errorMessage: string;

  constructor(private _developerService: DeveloperService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(
      (params: ParamMap) => (this.developerId = parseInt(params.get('id'), 10))
    );
    this._developerService.getDeveloper(this.developerId).subscribe(
      data => this.developer = data,
      error => this.errorMessage = error
    );
  }

  goBack() {
    this.router.navigate(['../../'], {relativeTo: this.route});
  }

  deleteDeveloper() {
    this._developerService.deleteDeveloper(this.developerId).subscribe(() => this.goBack());
  }

}
