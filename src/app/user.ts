import { IGame } from './game';

export interface IUser {
  id: number;
  userName: string;
  email: string;
  password: string;
  games: IGame[];
}
