import { Component, OnInit } from '@angular/core';
import { ILanguage } from '../language';
import { LanguageService } from '../language.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-language-list',
  templateUrl: './language-list.component.html',
  styleUrls: ['./language-list.component.scss']
})
export class LanguageListComponent implements OnInit {

  public languages: ILanguage[];
  public errorMessage: String;

  constructor(private _languageService: LanguageService, private router: Router, private route: ActivatedRoute) {
    this.errorMessage = '';
  }

  ngOnInit() {
    this._languageService.getLanguages().subscribe(
      data => this.languages = data,
      error => this.errorMessage = error
    );
  }

  showDetails(id: number) {
    this.router.navigate([id + '/view'], {relativeTo: this.route});
  }

  showUpdate(id: number) {
    this.router.navigate([id + '/update'], {relativeTo: this.route});
  }

  deleteLanguage(id: number) {
    this._languageService.deleteLanguage(id).subscribe(() => this.updateLanguages());
  }

  showCreate() {
    this.router.navigate(['create'], {relativeTo: this.route});
  }

  updateLanguages() {
    this._languageService.getLanguages().subscribe(
      data => this.languages = data,
      error => this.errorMessage = error
    );
  }
}
