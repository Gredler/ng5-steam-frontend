import { Component, OnInit } from '@angular/core';
import { IGame } from '../game';
import { GameService } from '../game.service';
import { LanguageService } from '../language.service';
import { ILanguage } from '../language';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-game-manage-language',
  templateUrl: './game-manage-language.component.html',
  styleUrls: ['./game-manage-language.component.scss']
})
export class GameManageLanguageComponent implements OnInit {

  public gameId: number;
  public game: IGame;
  public errorMessage: string;

  public allLanguages: ILanguage[];

  constructor(
    private _gameService: GameService,
    private _languageService: LanguageService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe(
      (params: ParamMap) => (this.gameId = parseInt(params.get('id'), 10))
    );
    this._gameService
      .getGame(this.gameId)
      .subscribe(
        data => (this.game = data),
        error => (this.errorMessage = error)
      );
    this._languageService
      .getLanguages()
      .subscribe(
        data => (this.allLanguages = data),
        error => (this.errorMessage = error)
      );
  }

  goBack() {
    this.router.navigate(['../../../'], { relativeTo: this.route });
  }

  showLanguage(id: number) {
    this.router.navigate(['../../../../languages/' + id + '/view'], {
      relativeTo: this.route
    });
  }

  updateGame() {
    this._gameService.updateGame(this.game).subscribe(() => this.goBack());
  }

  addLanguage(id: number) {
    if (!this.checkExist(id)) {
      let index = -1;
      this.allLanguages.forEach(g => {
        if ((g.id === id)) {
          index = this.allLanguages.indexOf(g);
        }
      });

      this.game.languages.push(this.allLanguages[index]);
    }
  }

  removeLanguage(id: number) {
      let index = -1;

      for (let i = 0; i < this.game.languages.length; i++) {
        if (id === this.game.languages[i].id) {
          index = i;
          break;
        }
      }

      if (index > -1) {
        this.game.languages.splice(index, 1);
      }
  }

  private checkExist(id: number) {
    let found = false;

    this.game.languages.forEach(g => {
      if (id === g.id) {
        found = true;
      }
    });
    return found;
  }
}
