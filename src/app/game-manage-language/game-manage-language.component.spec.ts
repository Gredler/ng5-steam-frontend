import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameManageLanguageComponent } from './game-manage-language.component';

describe('GameManageLanguageComponent', () => {
  let component: GameManageLanguageComponent;
  let fixture: ComponentFixture<GameManageLanguageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameManageLanguageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameManageLanguageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
