import { Component, OnInit } from '@angular/core';
import { IPublisher } from '../publisher';
import { PublisherService } from '../publisher.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-publisher-list',
  templateUrl: './publisher-list.component.html',
  styleUrls: ['./publisher-list.component.scss']
})
export class PublisherListComponent implements OnInit {

  public publishers: IPublisher[];
  public errorMessage: String;

  constructor(private _publisherService: PublisherService, private router: Router, private route: ActivatedRoute) {
    this.errorMessage = '';
  }

  ngOnInit() {
    this.updatePublishers();
  }

  showDetails(id: number) {
    this.router.navigate([id + '/view'], {relativeTo: this.route});
  }

  showUpdate(id: number) {
    this.router.navigate([id + '/update'], {relativeTo: this.route});
  }

  deletePublisher(id: number) {
    this._publisherService.deletePublisher(id).subscribe(() => this.updatePublishers());
  }

  showCreate() {
    this.router.navigate(['create'], {relativeTo: this.route});
  }

  updatePublishers() {
    this._publisherService.getPublishers().subscribe(
      data => this.publishers = data,
      error => this.errorMessage = error
    );
  }
}
