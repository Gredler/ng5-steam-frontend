import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameManageDeveloperComponent } from './game-manage-developer.component';

describe('GameManageDeveloperComponent', () => {
  let component: GameManageDeveloperComponent;
  let fixture: ComponentFixture<GameManageDeveloperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameManageDeveloperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameManageDeveloperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
