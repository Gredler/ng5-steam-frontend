import { Component, OnInit } from '@angular/core';
import { IGame } from '../game';
import { IDeveloper } from '../developer';
import { GameService } from '../game.service';
import { DeveloperService } from '../developer.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-game-manage-developer',
  templateUrl: './game-manage-developer.component.html',
  styleUrls: ['./game-manage-developer.component.scss']
})
export class GameManageDeveloperComponent implements OnInit {

  public gameId: number;
  public game: IGame;
  public errorMessage: string;

  public allDevelopers: IDeveloper[];

  constructor(
    private _gameService: GameService,
    private _developerService: DeveloperService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe(
      (params: ParamMap) => (this.gameId = parseInt(params.get('id'), 10))
    );
    this._gameService
      .getGame(this.gameId)
      .subscribe(
        data => (this.game = data),
        error => (this.errorMessage = error)
      );
    this._developerService
      .getDevelopers()
      .subscribe(
        data => (this.allDevelopers = data),
        error => (this.errorMessage = error)
      );
  }

  goBack() {
    this.router.navigate(['../../../'], { relativeTo: this.route });
  }

  showDeveloper(id: number) {
    this.router.navigate(['../../../../developers/' + id + '/view'], {
      relativeTo: this.route
    });
  }

  updateGame() {
    this._gameService.updateGame(this.game).subscribe(() => this.goBack());
  }

  addDeveloper(id: number) {
    if (!this.checkExist(id)) {
      let index = -1;
      this.allDevelopers.forEach(g => {
        if ((g.id === id)) {
          index = this.allDevelopers.indexOf(g);
        }
      });

      this.game.developers.push(this.allDevelopers[index]);    }
  }

  removeDeveloper(id: number) {
      let index = -1;

      for (let i = 0; i < this.game.developers.length; i++) {
        if (id === this.game.developers[i].id) {
          index = i;
          break;
        }
      }

      if (index > -1) {
        this.game.developers.splice(index, 1);
      }
  }

  private checkExist(id: number) {
    let found = false;

    this.game.developers.forEach(g => {
      if (id === g.id) {
        found = true;
      }
    });
    return found;
  }

}
