import { Injectable } from '@angular/core';
import { IPublisher } from './publisher';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';

@Injectable()
export class PublisherService {

  private _url: string;

  constructor(private http: HttpClient) {
    this._url = '/api/v1/publisher/';
  }

  getPublishers(): Observable<IPublisher[]> {
    return this.http.get<IPublisher[]>(this._url).catch(this.errorHandler);
  }

  getPublisher(id: number): Observable<IPublisher> {
    return this.http.get<IPublisher>(this._url + id).catch(this.errorHandler);
  }

  deletePublisher(id: number): Observable<IPublisher> {
    return this.http.delete<IPublisher>(this._url + id).catch(this.errorHandler);
  }

  createPublisher(publisher: IPublisher): Observable<IPublisher> {
    return this.http.post<IPublisher>(this._url, publisher).catch(this.errorHandler);
  }

  updatePublisher(publisher: IPublisher): Observable<IPublisher> {
    return this.http.put<IPublisher>(this._url + publisher.id, publisher).catch(this.errorHandler);
  }

  errorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || 'Server Error');
  }

}
