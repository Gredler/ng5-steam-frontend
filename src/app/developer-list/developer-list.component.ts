import { Component, OnInit } from '@angular/core';
import { IDeveloper } from '../developer';
import { DeveloperService } from '../developer.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-developer-list',
  templateUrl: './developer-list.component.html',
  styleUrls: ['./developer-list.component.scss']
})
export class DeveloperListComponent implements OnInit {

  public developers: IDeveloper[];
  public errorMessage: String;

  constructor(private _developerService: DeveloperService, private router: Router, private route: ActivatedRoute) {
    this.errorMessage = '';
  }

  ngOnInit() {
    this._developerService.getDevelopers().subscribe(
      data => this.developers = data,
      error => this.errorMessage = error
    );
  }

  showDetails(id: number) {
    this.router.navigate([id + '/view'], {relativeTo: this.route});
  }

  showUpdate(id: number) {
    this.router.navigate([id + '/update'], {relativeTo: this.route});
  }

  deleteDeveloper(id: number) {
    this._developerService.deleteDeveloper(id).subscribe(() => this.updateDevelopers());
  }

  showCreate() {
    this.router.navigate(['create'], {relativeTo: this.route});
  }

  updateDevelopers() {
    this._developerService.getDevelopers().subscribe(
      data => this.developers = data,
      error => this.errorMessage = error
    );
  }

}
