import { Component, OnInit } from '@angular/core';
import { IUser } from '../user';
import { UserService } from '../user.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { GameService } from '../game.service';
import { IGame } from '../game';

@Component({
  selector: 'app-user-game-manage',
  templateUrl: './user-game-manage.component.html',
  styleUrls: ['./user-game-manage.component.scss']
})
export class UserGameManageComponent implements OnInit {
  public userId: number;
  public user: IUser;
  public errorMessage: string;

  public allGames: IGame[];

  constructor(
    private _userService: UserService,
    private _gameService: GameService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe(
      (params: ParamMap) => (this.userId = parseInt(params.get('id'), 10))
    );
    this._userService
      .getUser(this.userId)
      .subscribe(
        data => (this.user = data),
        error => (this.errorMessage = error)
      );
    this._gameService
      .getGames()
      .subscribe(
        data => (this.allGames = data),
        error => (this.errorMessage = error)
      );
  }

  goBack() {
    this.router.navigate(['../../'], { relativeTo: this.route });
  }

  showGame(id: number) {
    this.router.navigate(['../../../games/' + id + '/view'], {
      relativeTo: this.route
    });
  }

  updateUser() {
    this._userService.updateUser(this.user).subscribe(() => this.goBack());
  }

  addGame(id: number) {
    if (!this.checkExist(id)) {
      let index = -1;
      this.allGames.forEach(g => {
        if ((g.id === id)) {
          index = this.allGames.indexOf(g);
        }
      });

      this.user.games.push(this.allGames[index]);
    }
  }

  removeGame(id: number) {
    let index = -1;

    for (let i = 0; i < this.user.games.length; i++) {
      if (id === this.user.games[i].id) {
        index = i;
        break;
      }
    }

    if (index > -1) {
      this.user.games.splice(index, 1);
    }
  }

  private checkExist(id: number) {
    let found = false;

    this.user.games.forEach(g => {
      if (id === g.id) {
        found = true;
      }
    });
    return found;
  }
}
