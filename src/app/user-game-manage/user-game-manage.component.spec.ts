import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserGameManageComponent } from './user-game-manage.component';

describe('UserGameManageComponent', () => {
  let component: UserGameManageComponent;
  let fixture: ComponentFixture<UserGameManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserGameManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserGameManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
