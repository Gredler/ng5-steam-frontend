import { Component, OnInit } from '@angular/core';
import { IPublisher } from '../publisher';
import { PublisherService } from '../publisher.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-publisher-update',
  templateUrl: './publisher-update.component.html',
  styleUrls: ['./publisher-update.component.scss']
})
export class PublisherUpdateComponent implements OnInit {

  public publisherId: number;
  public publisher: IPublisher;
  public errorMessage: string;

  constructor(private _publisherService: PublisherService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(
      (params: ParamMap) => (this.publisherId = parseInt(params.get('id'), 10))
    );
    this._publisherService.getPublisher(this.publisherId).subscribe(
      data => this.publisher = data,
      error => this.errorMessage = error
    );
  }

  goBack() {
    this.router.navigate(['../../'], {relativeTo: this.route});
  }

  updatePublisher() {
    this._publisherService.updatePublisher(this.publisher).subscribe(() => this.goBack());
  }

}
