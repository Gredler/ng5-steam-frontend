import { Component, OnInit } from '@angular/core';
import { IUser } from '../user';
import { UserService } from '../user.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.scss']
})
export class UserCreateComponent implements OnInit {

  public user: IUser;
  public errorMessage: string;

  constructor(private _userService: UserService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.user = {
      id: undefined,
      userName: '',
      email: '',
      games: [],
      password: ''
    };
  }

  goBack() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  createUser() {
    this._userService.createUser(this.user).subscribe(() => this.goBack());
  }

}
