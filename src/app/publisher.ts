export interface IPublisher {
  id: number;
  publisherName: string;
}
