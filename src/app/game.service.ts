import { Injectable } from '@angular/core';
import { IGame } from './game';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class GameService {

  private _url: string;

  constructor(private http: HttpClient) {
    this._url = '/api/v1/game/';
  }

  getGames(): Observable<IGame[]> {
    return this.http.get<IGame[]>(this._url).catch(this.errorHandler);
  }

  getGame(id: number): Observable<IGame> {
    return this.http.get<IGame>(this._url + id).catch(this.errorHandler);
  }

  deleteGame(id: number): Observable<IGame> {
    return this.http.delete<IGame>(this._url + id).catch(this.errorHandler);
  }

  createGame(game: IGame): Observable<IGame> {
    return this.http.post<IGame>(this._url, game).catch(this.errorHandler);
  }

  updateGame(game: IGame): Observable<IGame> {
    return this.http.put<IGame>(this._url + game.id, game).catch(this.errorHandler);
  }

  errorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || 'Server Error');
  }
}
