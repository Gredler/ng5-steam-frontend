import { IPublisher } from './publisher';
import { ILanguage } from './language';
import { IDeveloper } from './developer';
import { EGenre } from './egenre';

export interface IGame {
  id: number;
  description: string;
  gameName: string;
  price: number;
  releaseDate: Date;
  developers: IDeveloper[];
  publisher: IPublisher;
  languages: ILanguage[];
  genre: EGenre;
}
