import { ELanguage } from './elanguage';

export interface ILanguage {
  id: number;
  language: ELanguage;
}
