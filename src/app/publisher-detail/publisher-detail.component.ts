import { Component, OnInit } from '@angular/core';
import { PublisherService } from '../publisher.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { IPublisher } from '../publisher';

@Component({
  selector: 'app-publisher-detail',
  templateUrl: './publisher-detail.component.html',
  styleUrls: ['./publisher-detail.component.scss']
})
export class PublisherDetailComponent implements OnInit {

  public publisherId: number;
  public publisher: IPublisher;
  public errorMessage: string;

  constructor(private _publisherService: PublisherService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(
      (params: ParamMap) => (this.publisherId = parseInt(params.get('id'), 10))
    );
    this._publisherService.getPublisher(this.publisherId).subscribe(
      data => this.publisher = data,
      error => this.errorMessage = error
    );
  }

  goBack() {
    this.router.navigate(['../../'], {relativeTo: this.route});
  }

  deletePublisher() {
    this._publisherService.deletePublisher(this.publisherId).subscribe(() => this.goBack());
  }
}
