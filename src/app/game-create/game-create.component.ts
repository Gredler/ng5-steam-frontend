import { Component, OnInit } from '@angular/core';
import { IGame } from '../game';
import { IPublisher } from '../publisher';
import { GameService } from '../game.service';
import { PublisherService } from '../publisher.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { EGenre } from '../egenre';

@Component({
  selector: 'app-game-create',
  templateUrl: './game-create.component.html',
  styleUrls: ['./game-create.component.scss']
})
export class GameCreateComponent implements OnInit {

  public game: IGame;
  public errorMessage: string;
  public allPublishers: IPublisher[];
  public allGenres;

  constructor(
    private _gameService: GameService,
    private _publisherService: PublisherService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.allGenres = Object.keys(EGenre);
    this.allGenres = this.allGenres.slice(this.allGenres.length / 2);

    this.game = {
      id: null,
      gameName: '',
      description: '',
      developers: null,
      genre: EGenre.ADVENTURE,
      languages: null,
      price: 0,
      publisher: null,
      releaseDate: null,
    };
  }

  ngOnInit() {
    this._publisherService
      .getPublishers()
      .subscribe(
        data => (this.allPublishers = data),
        error => (this.errorMessage = error)
      );
  }

  goBack() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  createGame() {
    const selected = $('#sel-publisher').val()[0];

    if (selected != null) {
      this.game.publisher = this.allPublishers[selected];
    }

    this._gameService.createGame(this.game).subscribe(() => this.goBack());
  }

}
