import { Injectable } from '@angular/core';
import { ILanguage } from './language';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class LanguageService {

  private _url: string;

  constructor(private http: HttpClient) {
    this._url = '/api/v1/language/';
  }

  getLanguages(): Observable<ILanguage[]> {
    return this.http.get<ILanguage[]>(this._url).catch(this.errorHandler);
  }

  getLanguage(id: number): Observable<ILanguage> {
    return this.http.get<ILanguage>(this._url + id).catch(this.errorHandler);
  }

  deleteLanguage(id: number): Observable<ILanguage> {
    return this.http.delete<ILanguage>(this._url + id).catch(this.errorHandler);
  }

  createLanguage(language: ILanguage): Observable<ILanguage> {
    return this.http.post<ILanguage>(this._url, language).catch(this.errorHandler);
  }

  updateLanguage(language: ILanguage): Observable<ILanguage> {
    return this.http.put<ILanguage>(this._url + language.id, language).catch(this.errorHandler);
  }

  errorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || 'Server Error');
  }
}
