export interface IDeveloper {
  id: number;
  developerName: string;
}
