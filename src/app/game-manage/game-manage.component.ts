import { Component, OnInit } from '@angular/core';
import { IGame } from '../game';
import { GameService } from '../game.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-game-manage',
  templateUrl: './game-manage.component.html',
  styleUrls: ['./game-manage.component.scss']
})
export class GameManageComponent implements OnInit {

  public gameId: number;
  public game: IGame;
  public errorMessage: string;

  constructor(private _gameService: GameService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(
      (params: ParamMap) => (this.gameId = parseInt(params.get('id'), 10))
    );
    this._gameService.getGame(this.gameId).subscribe(
      data => this.game = data,
      error => this.errorMessage = error
    );
  }

  goBack() {
    this.router.navigate(['../../'], {relativeTo: this.route});
  }

  deleteGame() {
    this._gameService.deleteGame(this.gameId).subscribe(() => this.goBack());
  }

  showManageDevelopers() {
    this.router.navigate(['developers'], {relativeTo: this.route});
  }

  showManageLanguages() {
    this.router.navigate(['languages'], {relativeTo: this.route});
  }
}
