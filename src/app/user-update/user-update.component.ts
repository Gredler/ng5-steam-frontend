import { Component, OnInit } from '@angular/core';
import { IUser } from '../user';
import { UserService } from '../user.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.scss']
})
export class UserUpdateComponent implements OnInit {

  public userId: number;
  public user: IUser;
  public errorMessage: string;

  constructor(private _userService: UserService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(
      (params: ParamMap) => (this.userId = parseInt(params.get('id'), 10))
    );
    this._userService.getUser(this.userId).subscribe(
      data => this.user = data,
      error => this.errorMessage = error
    );
  }

  goBack() {
    this.router.navigate(['../../'], {relativeTo: this.route});
  }

  updateUser() {
    this._userService.updateUser(this.user).subscribe(() => this.goBack());
  }

}
