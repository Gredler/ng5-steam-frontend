import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PublisherListComponent } from './publisher-list/publisher-list.component';
import { GameListComponent } from './game-list/game-list.component';
import { UserListComponent } from './user-list/user-list.component';
import { DeveloperListComponent } from './developer-list/developer-list.component';
import { LanguageListComponent } from './language-list/language-list.component';
import { PublisherDetailComponent } from './publisher-detail/publisher-detail.component';
import { PublisherUpdateComponent } from './publisher-update/publisher-update.component';
import { PublisherCreateComponent } from './publisher-create/publisher-create.component';
import { DeveloperViewComponent } from './developer-view/developer-view.component';
import { DeveloperUpdateComponent } from './developer-update/developer-update.component';
import { DeveloperCreateComponent } from './developer-create/developer-create.component';
import { LanguageDetailComponent } from './language-detail/language-detail.component';
import { LanguageUpdateComponent } from './language-update/language-update.component';
import { LanguageCreateComponent } from './language-create/language-create.component';
import { GameDetailComponent } from './game-detail/game-detail.component';
import { GameUpdateComponent } from './game-update/game-update.component';
import { GameCreateComponent } from './game-create/game-create.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserUpdateComponent } from './user-update/user-update.component';
import { UserCreateComponent } from './user-create/user-create.component';
import { UserGameManageComponent } from './user-game-manage/user-game-manage.component';
import { GameManageComponent } from './game-manage/game-manage.component';
import { GameManageLanguageComponent } from './game-manage-language/game-manage-language.component';
import { GameManageDeveloperComponent } from './game-manage-developer/game-manage-developer.component';

const routes: Routes = [
  { path: '', redirectTo: '/games', pathMatch: 'full' },
  { path: 'publishers', component: PublisherListComponent },
  { path: 'publishers/:id/view', component: PublisherDetailComponent },
  { path: 'publishers/:id/update', component: PublisherUpdateComponent },
  { path: 'publishers/create', component: PublisherCreateComponent },
  { path: 'games', component: GameListComponent },
  { path: 'games/:id/view', component: GameDetailComponent },
  { path: 'games/:id/update', component: GameUpdateComponent },
  { path: 'games/:id/manage', component: GameManageComponent },
  { path: 'games/:id/manage/languages', component: GameManageLanguageComponent },
  { path: 'games/:id/manage/developers', component: GameManageDeveloperComponent },
  { path: 'games/create', component: GameCreateComponent },
  { path: 'users', component: UserListComponent },
  { path: 'users/:id/view', component: UserDetailComponent },
  { path: 'users/:id/update', component: UserUpdateComponent },
  { path: 'users/:id/manage-games', component: UserGameManageComponent },
  { path: 'users/create', component: UserCreateComponent },
  { path: 'developers', component: DeveloperListComponent },
  { path: 'developers/:id/view', component: DeveloperViewComponent },
  { path: 'developers/:id/update', component: DeveloperUpdateComponent },
  { path: 'developers/create', component: DeveloperCreateComponent },
  { path: 'languages', component: LanguageListComponent },
  { path: 'languages/:id/view', component: LanguageDetailComponent },
  { path: 'languages/:id/update', component: LanguageUpdateComponent },
  { path: 'languages/create', component: LanguageCreateComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
