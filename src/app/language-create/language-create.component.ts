import { Component, OnInit } from '@angular/core';
import { ILanguage } from '../language';
import { LanguageService } from '../language.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ELanguage } from '../elanguage';

@Component({
  selector: 'app-language-create',
  templateUrl: './language-create.component.html',
  styleUrls: ['./language-create.component.scss']
})
export class LanguageCreateComponent implements OnInit {

  public language: ILanguage;
  public errorMessage: string;
  public allLanguages;

  constructor(private _languageService: LanguageService, private router: Router, private route: ActivatedRoute) {
    this.language = {
      id: undefined,
      language: -1
    };

    this.allLanguages = Object.keys(ELanguage);
    this.allLanguages = this.allLanguages.slice(this.allLanguages.length / 2);
  }

  ngOnInit() {

  }

  goBack() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  createLanguage() {
    this._languageService.createLanguage(this.language).subscribe(() => this.goBack());
  }

}
