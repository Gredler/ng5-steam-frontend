import { Injectable } from '@angular/core';
import { IUser } from './user';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class UserService {

  private _url: string;

  constructor(private http: HttpClient) {
    this._url = '/api/v1/user/';
  }

  getUsers(): Observable<IUser[]> {
    return this.http.get<IUser[]>(this._url).catch(this.errorHandler);
  }

  getUser(id: number): Observable<IUser> {
    return this.http.get<IUser>(this._url + id).catch(this.errorHandler);
  }

  deleteUser(id: number): Observable<IUser> {
    return this.http.delete<IUser>(this._url + id).catch(this.errorHandler);
  }

  createUser(user: IUser): Observable<IUser> {
    return this.http.post<IUser>(this._url, user).catch(this.errorHandler);
  }

  updateUser(user: IUser): Observable<IUser> {
    return this.http.put<IUser>(this._url + user.id, user).catch(this.errorHandler);
  }

  errorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || 'Server Error');
  }
}
