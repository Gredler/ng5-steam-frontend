import { Component, OnInit } from '@angular/core';
import { IGame } from '../game';
import { GameService } from '../game.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { PublisherService } from '../publisher.service';
import { IPublisher } from '../publisher';
import { EGenre } from '../egenre';

@Component({
  selector: 'app-game-update',
  templateUrl: './game-update.component.html',
  styleUrls: ['./game-update.component.scss']
})
export class GameUpdateComponent implements OnInit {
  public gameId: number;
  public game: IGame;
  public errorMessage: string;
  public allPublishers: IPublisher[];
  public allGenres;

  constructor(
    private _gameService: GameService,
    private _publisherService: PublisherService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.allGenres = Object.keys(EGenre);
    this.allGenres = this.allGenres.slice(this.allGenres.length / 2);
  }

  ngOnInit() {
    this.route.paramMap.subscribe(
      (params: ParamMap) => (this.gameId = parseInt(params.get('id'), 10))
    );
    this._gameService
      .getGame(this.gameId)
      .subscribe(
        data => (this.game = data),
        error => (this.errorMessage = error)
      );
    this._publisherService
      .getPublishers()
      .subscribe(
        data => (this.allPublishers = data),
        error => (this.errorMessage = error)
      );
  }

  goBack() {
    this.router.navigate(['../../'], { relativeTo: this.route });
  }

  updateGame() {
    const selected = $('#sel-publisher').val()[0];

    if (selected != null) {
      this.game.publisher = this.allPublishers[selected];
    }

    this._gameService.updateGame(this.game).subscribe(() => this.goBack());
  }
}
