import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';

import * as $ from 'jquery';

import { AppComponent } from './app.component';
import { PublisherListComponent } from './publisher-list/publisher-list.component';
import { HttpClientModule } from '@angular/common/http';
import { PublisherService } from './publisher.service';
import { GameListComponent } from './game-list/game-list.component';
import { UserListComponent } from './user-list/user-list.component';
import { DeveloperListComponent } from './developer-list/developer-list.component';
import { LanguageListComponent } from './language-list/language-list.component';
import { GameService } from './game.service';
import { LanguageService } from './language.service';
import { DeveloperService } from './developer.service';
import { UserService } from './user.service';
import { PublisherDetailComponent } from './publisher-detail/publisher-detail.component';
import { PublisherUpdateComponent } from './publisher-update/publisher-update.component';
import { PublisherCreateComponent } from './publisher-create/publisher-create.component';
import { DeveloperViewComponent } from './developer-view/developer-view.component';
import { DeveloperCreateComponent } from './developer-create/developer-create.component';
import { DeveloperUpdateComponent } from './developer-update/developer-update.component';
import { LanguageDetailComponent } from './language-detail/language-detail.component';
import { LanguageCreateComponent } from './language-create/language-create.component';
import { LanguageUpdateComponent } from './language-update/language-update.component';
import { GameDetailComponent } from './game-detail/game-detail.component';
import { GameUpdateComponent } from './game-update/game-update.component';
import { GameCreateComponent } from './game-create/game-create.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserUpdateComponent } from './user-update/user-update.component';
import { UserCreateComponent } from './user-create/user-create.component';
import { UserGameManageComponent } from './user-game-manage/user-game-manage.component';
import { GameManageComponent } from './game-manage/game-manage.component';
import { GameManageLanguageComponent } from './game-manage-language/game-manage-language.component';
import { GameManageDeveloperComponent } from './game-manage-developer/game-manage-developer.component';


@NgModule({
  declarations: [
    AppComponent,
    PublisherListComponent,
    GameListComponent,
    UserListComponent,
    DeveloperListComponent,
    LanguageListComponent,
    PublisherDetailComponent,
    PublisherUpdateComponent,
    PublisherCreateComponent,
    DeveloperViewComponent,
    DeveloperCreateComponent,
    DeveloperUpdateComponent,
    LanguageDetailComponent,
    LanguageCreateComponent,
    LanguageUpdateComponent,
    GameDetailComponent,
    GameUpdateComponent,
    GameCreateComponent,
    UserDetailComponent,
    UserUpdateComponent,
    UserCreateComponent,
    UserGameManageComponent,
    GameManageComponent,
    GameManageLanguageComponent,
    GameManageDeveloperComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    PublisherService,
    GameService,
    LanguageService,
    DeveloperService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
