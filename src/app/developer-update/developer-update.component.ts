import { Component, OnInit } from '@angular/core';
import { IDeveloper } from '../developer';
import { DeveloperService } from '../developer.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-developer-update',
  templateUrl: './developer-update.component.html',
  styleUrls: ['./developer-update.component.scss']
})
export class DeveloperUpdateComponent implements OnInit {

  public developerId: number;
  public developer: IDeveloper;
  public errorMessage: string;

  constructor(private _developerService: DeveloperService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(
      (params: ParamMap) => (this.developerId = parseInt(params.get('id'), 10))
    );
    this._developerService.getDeveloper(this.developerId).subscribe(
      data => this.developer = data,
      error => this.errorMessage = error
    );
  }

  goBack() {
    this.router.navigate(['../../'], {relativeTo: this.route});
  }

  updateDeveloper() {
    this._developerService.updateDeveloper(this.developer).subscribe(() => this.goBack());
  }
}
