import { Component, OnInit } from '@angular/core';
import { IPublisher } from '../publisher';
import { PublisherService } from '../publisher.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-publisher-create',
  templateUrl: './publisher-create.component.html',
  styleUrls: ['./publisher-create.component.scss']
})
export class PublisherCreateComponent implements OnInit {

  public publisher: IPublisher;
  public errorMessage: string;

  constructor(private _publisherService: PublisherService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.publisher = {
      id: undefined,
      publisherName: ''
    };
  }

  goBack() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  createPublisher() {
    this._publisherService.createPublisher(this.publisher).subscribe(() => this.goBack());
  }

}
