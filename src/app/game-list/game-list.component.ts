import { Component, OnInit } from '@angular/core';
import { IGame } from '../game';
import { GameService } from '../game.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.scss']
})
export class GameListComponent implements OnInit {

  public games: IGame[];
  public errorMessage: String;

  constructor(private _gameService: GameService, private router: Router, private route: ActivatedRoute) {
    this.errorMessage = '';
  }

  ngOnInit() {
    this._gameService.getGames().subscribe(
      data => this.games = data,
      error => this.errorMessage = error
    );
  }

  showDetails(id: number) {
    this.router.navigate([id + '/view'], {relativeTo: this.route});
  }

  showUpdate(id: number) {
    this.router.navigate([id + '/update'], {relativeTo: this.route});
  }

  deleteDeveloper(id: number) {
    this._gameService.deleteGame(id).subscribe(() => this.updateGames());
  }

  showCreate() {
    this.router.navigate(['create'], {relativeTo: this.route});
  }

  showManage(id: number) {
    this.router.navigate([id + '/manage'], {relativeTo: this.route});
  }

  updateGames() {
    this._gameService.getGames().subscribe(
      data => this.games = data,
      error => this.errorMessage = error
    );
  }

}
