import { Component, OnInit } from '@angular/core';
import { IDeveloper } from '../developer';
import { DeveloperService } from '../developer.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-developer-create',
  templateUrl: './developer-create.component.html',
  styleUrls: ['./developer-create.component.scss']
})
export class DeveloperCreateComponent implements OnInit {

  public developer: IDeveloper;
  public errorMessage: string;

  constructor(private _developerService: DeveloperService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.developer = {
      id: undefined,
      developerName: ''
    };
  }

  goBack() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  createDeveloper() {
    this._developerService.createDeveloper(this.developer).subscribe(() => this.goBack());
  }

}
