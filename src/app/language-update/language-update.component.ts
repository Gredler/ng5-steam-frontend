import { Component, OnInit } from '@angular/core';
import { ILanguage } from '../language';
import { LanguageService } from '../language.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ELanguage } from '../elanguage';

@Component({
  selector: 'app-language-update',
  templateUrl: './language-update.component.html',
  styleUrls: ['./language-update.component.scss']
})
export class LanguageUpdateComponent implements OnInit {

  public language: ILanguage;
  public errorMessage: string;
  public allLanguages;
  public languageId: number;

  constructor(private _languageService: LanguageService, private router: Router, private route: ActivatedRoute) {
    this.allLanguages = Object.keys(ELanguage);
    this.allLanguages = this.allLanguages.slice(this.allLanguages.length / 2);
  }

  ngOnInit() {
    this.route.paramMap.subscribe(
      (params: ParamMap) => (this.languageId = parseInt(params.get('id'), 10))
    );
    this._languageService.getLanguage(this.languageId).subscribe(
      data => this.language = data,
      error => this.errorMessage = error
    );
  }

  goBack() {
    this.router.navigate(['../../'], {relativeTo: this.route});
  }

  updateLanguage() {
    this._languageService.updateLanguage(this.language).subscribe(() => this.goBack());
  }
}
