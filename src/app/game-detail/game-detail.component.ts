import { Component, OnInit } from '@angular/core';
import { IGame } from '../game';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { GameService } from '../game.service';

@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.component.html',
  styleUrls: ['./game-detail.component.scss']
})
export class GameDetailComponent implements OnInit {

  public gameId: number;
  public game: IGame;
  public errorMessage: string;

  constructor(private _gameService: GameService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(
      (params: ParamMap) => (this.gameId = parseInt(params.get('id'), 10))
    );
    this._gameService.getGame(this.gameId).subscribe(
      data => this.game = data,
      error => this.errorMessage = error
    );
  }

  goBack() {
    this.router.navigate(['../../'], {relativeTo: this.route});
  }

  deleteGame() {
    this._gameService.deleteGame(this.gameId).subscribe(() => this.goBack());
  }

  showPublisher(id: number) {
    this.router.navigate(['../../../publishers/' + id + '/view'], {relativeTo: this.route});
  }

  showDeveloper(id: number) {
    this.router.navigate(['../../../developers/' + id + '/view'], {relativeTo: this.route});
  }

  showLanguage(id: number) {
    this.router.navigate(['../../../languages/' + id + '/view'], {relativeTo: this.route});
  }

}
